//EXERCICE 1
var valeur = prompt("Afficher un titre :");
document.getElementById("h2").innerHTML = "<p style='color:red'>"+valeur+"</p>";
document.getElementById("myId").innerHTML = "<p>Un message apparait</p>";


//EXERCICE 2
var nomExterne = "Hein "; 
var prenom;
function portee(nom) { 
 prenom = "Terieur "; 
 nomGlobale = "Halle "; 
 console.log(window.nomGlobale + nom + prenom); 
 console.log(nomGlobale + nomExterne + prenom); 
} 
portee("Ex "); 
//console.log(prenom);// provoque une erreur 

//EXERCICE 3

function carre(){
    var longueur = document.getElementById('longueurCarre');
    var perimetre;
    var missValue = document.getElementById('missValueCarre');
    //Si le champ est vide
    if (longueur.value.length==0){
        event.preventDefault();
        missValue.textContent = 'champ manquant';
        missValue.style.color = 'red';
    } else{
        perimetre=4*longueur.value;
        document.getElementById("resultatCarre").innerHTML = "<p>Perimetre = "+perimetre+"</p>";
    }
}
function rectangle(){
    var longueur = document.getElementById('longueurRectangle');
    var largeur = document.getElementById('largeurRectangle');
    var perimetre;
    var missValue = document.getElementById('missValueRectangle');
    //Si le champ est vide
    if (longueur.value.length==0){
        event.preventDefault();
        missValue.textContent = 'champ manquant';
        missValue.style.color = 'red';
    }else if(largeur.value.length==0){
        event.preventDefault();
        missValue.textContent = 'champ manquant';
        missValue.style.color = 'red';
    } else{
        perimetre=(2*longueur.value)+(2*largeur.value);
        document.getElementById("resultatRectangle").innerHTML = "<p>Perimetre = "+perimetre+"</p>";
    }
}
function triangle(){
    var longueur = document.getElementById('longueurTriangle');
    var perimetre;
    var missValue = document.getElementById('missValueTriangle');
    //Si le champ est vide
    if (longueur.value.length==0){
        event.preventDefault();
        missValue.textContent = 'champ manquant';
        missValue.style.color = 'red';
    } else{
        perimetre=3*longueur.value;
        document.getElementById("resultatTriangle").innerHTML = "<p>Perimetre = "+perimetre+"</p>";
    }
}
function polygone(){
    var longueur = document.getElementById('longueurPolygone');
    var nombreCote = document.getElementById('cotePolygone');
    var perimetre;
    var missValue = document.getElementById('missValuePolygone');
    //Si le champ est vide
    if (longueur.value.length==0){
        event.preventDefault();
        missValue.textContent = 'champ manquant';
        missValue.style.color = 'red';
    }else if(nombreCote.value.length==0){
        event.preventDefault();
        missValue.textContent = 'champ manquant';
        missValue.style.color = 'red';
    } else{
        perimetre=nombreCote.value*longueur.value;
        document.getElementById("resultatPolygone").innerHTML = "<p>Perimetre = "+perimetre+"</p>";
    }
}
//EXERCICE 4
function check() {
    var inputs = document.getElementsByTagName('input'),
        inputsLength = inputs.length;

    for (var i = 0; i < inputsLength; i++) {
        if (inputs[i].type === 'radio' && inputs[i].checked) {
            document.getElementById("choix").innerHTML = "<p>"+inputs[i].value+"</p>";
        }
    }
}

//EXERCICE 5
let d1 = document.getElementById("d1");
let d2 = document.getElementById("d2");
let p1 = document.getElementById("p1");
let l1 = document.getElementById("l1");

d1.addEventListener("mouseover", () => {p1.style.display = "block";});
d1.addEventListener("mouseout", () => {p1.style.display = "none";});
function togg(){
    if(getComputedStyle(d1).display != "none"){
      d1.style.display = "none";
    } else {
      d1.style.display = "block";
    }
  };
  togg1.onclick = togg;



//EXERCICE 6
var startTime = 0
var start = 0
var end = 0
var diff = 0
var timerID = 0
window.onload = chronoStart;
function chrono(){
	end = new Date()
	diff = end - start
	diff = new Date(diff)
	var msec = diff.getMilliseconds()
	var sec = diff.getSeconds()
	var min = diff.getMinutes()
	var hr = diff.getHours()-1
	if (min < 10){
		min = "0" + min
	}
	if (sec < 10){
		sec = "0" + sec
	}
	if(msec < 10){
		msec = "00" +msec
	}
	else if(msec < 100){
		msec = "0" +msec
	}
	document.getElementById("chronotime").value = hr + ":" + min + ":" + sec + ":" + msec
	timerID = setTimeout("chrono()", 10)
}
function chronoStart(){
	document.chronoForm.startstop.value = "stop!"
	document.chronoForm.startstop.onclick = chronoStop
	document.chronoForm.reset.onclick = chronoReset
	start = new Date()
	chrono()
}
function chronoContinue(){
	document.chronoForm.startstop.value = "stop!"
	document.chronoForm.startstop.onclick = chronoStop
	document.chronoForm.reset.onclick = chronoReset
	start = new Date()-diff
	start = new Date(start)
	chrono()
}
function chronoReset(){
	document.getElementById("chronotime").value = "0:00:00:000"
	start = new Date()
}
function chronoStopReset(){
	document.getElementById("chronotime").value = "0:00:00:000"
	document.chronoForm.startstop.onclick = chronoStart
}
function chronoStop(){
	document.chronoForm.startstop.value = "start!"
	document.chronoForm.startstop.onclick = chronoContinue
	document.chronoForm.reset.onclick = chronoStopReset
	clearTimeout(timerID)
}
//EXERCICE 7

function myFunction() {
    var form = document.getElementById('fname');
    var missPrenom = document.getElementById('missPrenom');
    if (form.value.length<2){
        missPrenom.textContent='La chaine doit comporter au moins 2 caracteres';
    }else{
        alert("Vous avez saisie : "+form.value);
    }
    
  }


  var formValid = document.getElementById('bouton_envoi');
  var prenom2 = document.getElementById('prenom2');
  var missPrenom2 = document.getElementById('missPrenom2');
  
  formValid.addEventListener('click', validation);
  
  function validation(event){
      //Si le champ est vide
      if (prenom2.validity.valueMissing){
          event.preventDefault();
          missPrenom2.textContent = 'champ manquant';
          missPrenom2.style.color = 'red';
      }else if(prenom2.value.length<2){
        event.preventDefault();
        missPrenom2.textContent = 'La chaine doit comporter au moins 2 caracteres';
        missPrenom2.style.color = 'red';
      } else{
          alert('Vous avez saisie : '+prenom2.value);
      }
  }
//EXERCICE 8


function getParameters()
{
var urlParams,
match,
pl = /+/g, // Regex for replacing addition symbol with a space
search = /([^&=]+)=?([^&]*)/g,
decode = function (s) { return decodeURIComponent(s.replace(pl, )); },
query = window.location.search.substring(1);
urlParams = {};
while (match = search.exec(query))
urlParams[decode(match[1])] = decode(match[2]);
document.getElementById("Params").innerHTML = "<p>Params : "+urlParams+"</p>";
return urlParams;
}




/////////////
var formValid2 = document.getElementById('bouton_envoi2');
var nombre = document.getElementById('nombre');
var nom = document.getElementById('nom');
var missNombre = document.getElementById('missNombre');
var missNom = document.getElementById('missNom');
var stockNombre;
var stockNom;

formValid2.addEventListener('click', validation2);

function validation2(event){
    //Si le champ est vide
    if (nombre.value.length==0){
        event.preventDefault();
        missNombre.textContent = 'champ manquant';
        missNombre.style.color = 'red';
    }else if(nom.value.length==0){
        event.preventDefault();
        missNom.textContent = 'champ manquant';
        missNom.style.color = 'red';
    }else {
        stockNombre=nombre.value;
        stockNom=nom.value;
        var WindowObjectReference; // variable globale
        window.open("Page2.html");
        
    }
}




//EXERCICE 9

function APPcodeNAME() {
    document.getElementById("APPcodeNAME").innerHTML = "<p>APPcodeNAME : "+navigator.appCodeName+"</p>";
}
function APPname() {
    document.getElementById("APPname").innerHTML = "<p> APPname : "+navigator.appName+"</p>";}
function APPversion() {
    document.getElementById("APPversion").innerHTML = "<p> APPversion : "+navigator.appVersion+"</p>";
}
function USERagent() {
    document.getElementById("USERagent").innerHTML = "<p> USERagent : "+navigator.userAgent+"</p>";}

